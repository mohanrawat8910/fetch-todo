import React, { Component } from "react";

export default class Table extends Component {
  deleteFunction = (index) => {
    this.props.DeleteF(index);
  };

  render() {
    console.log("receved", this.props.myData);
    const { myData } = this.props;
    console.log("myData", myData);

    const row = myData.map((item, index) => {
      console.log("This :", item);
      const comp = item.completed ? (
        <td style={{ color: "black" }}>completed</td>
      ) : (
        <td style={{ color: "red" }}>Not completed</td>
      );

      return (
        <tr key={index}>
          <th scope="row">{item.userId}</th>
          <td>{item.id}</td>
          <td>{item.title}</td>

          {comp}

          <td>
            <button
              onCliked="handleDelete"
              onClick={() => this.deleteFunction(index)}
            >
              delete
            </button>
          </td>
        </tr>
      );
    });

    return (
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th scope="col">User_id</th>
              <th scope="col">Id</th>
              <th scope="col">Title </th>
              <th scope="col">Completed</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>{row}</tbody>
        </table>
      </div>
    );
  }
}
