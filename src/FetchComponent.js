import React, { Component } from "react";
import axios from "axios";

import Table from "./components/Table";
export default class FetchComponent extends Component {
  state = {
    dataSet:[],
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then((response) => {
        // handle success
        console.log("response", response);
        if (response.status.toString() === "200") {
          let _data = response.data;

          this.setState({
            dataSet: _data,
          });
        }
        console.log("dataset", this.state.dataSet);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }



//   handleDelete  

handleDelete = (e_index) => {
    console.log("deletefuncall", e_index);
    let { dataSet } = this.state;
    dataSet.splice(e_index, 1);
    this.setState({ dataSet });
  };

  render() {
    // const { dataSet } = this.state;

    // let allData = dataSet;
    // console.log("allData", allData);

    // const todoDatas = (allData.map = (item, index) => {
    //   return (
    //     <>
    //       <h5 key={index}>{item.title}</h5>
    //     </>
    //   );
    // });

    return (
      <>
        <h1 style={{textAlign: 'center'}}>FETCH DATA TODO</h1>
        <Table myData={this.state.dataSet} DeleteF={this.handleDelete}/>
      </>
    );
  }
}
